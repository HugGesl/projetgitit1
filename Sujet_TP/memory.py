import numpy as np

import random

"""
@author vcoindet
Jeu de memory
"""

Tabl = ['a', 'a', 'b', 'b', 'c', 'c', 'd', 'd', 'e', 'e', 'f', 'f']

def melange_carte(Tab):
    """
    Fonction qui mélange les cartes du jeux
    :param Tab: liste de str
    :return: liste de str melangé
    """
    for i in range(1, len(Tab)):
        # pick an element in Tab[:i+1] with which to exchange Tab[i]
        j = int(random.random() * (i+1))
        Tab[i], Tab[j] = Tab[j], Tab[i]
    return Tab

def carte_cache(Tab):   #TODO
    """
    Doit créer une liste de même longueur que la liste renvoyés dans la
    question précédentes
    :param Tab: lst de str
    :return: lst
    """
    n=len(Tab)
    L=np.zeros((1,n))
    return L[0]
        

def choisir_cartes(Tab):
    """
    Fonction qui laisse l'utilisateur choisir ces cartes
    :param Tab: liste de str
    :return: un couple de carte
    """
    n=len(Tab)
    c1 = input("Choisissez une carte : ")
    while (c1 is not int) or (c1 is not float) or c1>=n:
        c1 = input("Erreur, veuillez entrer un nombre du tableau!")
    c1=int(c1)
    print(Tab[c1])
    c2 = input(int("Choisissez une deuxieme carte : "))
    while (c1 is not int) or (c1 is not float) or c1>=n or c1 == c2:
        c2 = input("Erreur, la deuxieme carte ne peut être la même que la premiere et être un nombre du tableau ! ")
    c2=int(c2)
    print(Tab[c2])
    return [c1,c2]


def retourne_carte (c1, c2, Tab, Tab_cache):    #TODO
    """
    Doit retrouner les cartes dans la liste cachée
    :param c1: str, une carte
    :param c2: str, une carte
    :param Tab: lstd e str
    :param Tab_cache: lst str
    :return: carte
    """
    C=[]
    for i in range(len(Tab_cache)):
        if Tab_cache[i]==c1:
            C.append(c1)
        elif Tab_cache[i]==c2:
            C.append(c2)
    return C
    


def jouer(Tab):
    """
    Fonction qui joue
    :param Tab: lst de str
    :return:
    """
    print("Bienvenue sur notre Jeu Memory, ci dessous voici les cartes mise en jeu. Il y en a 12")
    print(Tab)
    Tab = melange_carte(Tab)
    print("Maintenant je vais melanger les cartes et les retourner")
    
    Tab_cache =  carte_cache(Tab)
    print(Tab_cache)

    [c1, c2] = choisir_cartes(Tab)
    Tab_cache = retourne_carte(c1, c2, Tab, Tab_cache)
    print(Tab_cache)
    
    while 0 in Tab_cache:
        [c1, c2] = choisir_cartes(Tab)
        
        Tab_cache = retourne_carte(c1, c2, Tab, Tab_cache)
        
        print(Tab_cache)
        
    print("Bravo tu as gagné!!!")

    
# jouer(Tabl)

if __name__ == "__main__":
    print(melange_carte(Tabl))
    c = carte_cache(Tabl)
    print(c)
    print(retourne_carte(1, 3, Tabl, c))
          